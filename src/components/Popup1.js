import React , { useRef } from 'react';
import Popup from "reactjs-popup";
import { addNote } from "../actions";
import { connect } from 'react-redux';

const Popup1 = (props) =>{

	var month = props.month+1;
	month = (month > 9) ? month : '0'+month; 
	console.log("Popup1",props, month);
	const inputTitle = useRef(null);
	const inputContent = useRef(null);
	const inputDate = useRef(null);
	let hrefLink = '#';
	// var d = new Date(props.year, props.month+1, props.date);

	const handlePopup = () =>
	{
		var title = inputTitle.current.value;
		var content = inputTitle.current.value;
		var date = inputDate.current.value;
		if(title !== '' && date !== '')
		{
			props.dispatch(addNote(title,content,date));
			// props.dispatch(props.handleBlankDate);
			props.togglePopup();
		}
	}

	return (
		<Popup defaultOpen={true} position="top left" closeOnDocumentClick={true} repositionOnResize={true} onClose={props.handleBlankDate}>
		   {close => (
		      <div>
		        <input ref={inputTitle} placeholder="enter your note title........" /><br/>
		        <textarea ref={inputContent} placeholder="enter your note content........"></textarea><br/>
		        <input type="hidden" ref={inputDate} value={props.year+'-'+month+'-'+props.date} />
		        <input type="button" onClick={handlePopup} value="create note" /><br/>
		        <a className="close" onClick={close} href={hrefLink}>
		          &times;
		        </a>
		      </div>
		    )}
		</Popup>
	)
}

function mapStateToProps(state)
{
  return state;
}

export default connect(mapStateToProps)(Popup1);

// export default (props) => (
//   <Popup trigger={<button className="button"> Open Modal </button>} modal>
// 	{console.log("Popup1",props)}
//     {close => (
//       <div className="modal">
//         <a href="#" className="close" onClick={close}>
//           &times;
//         </a>
//         <div className="header"> Modal Title </div>
//         <div className="content">
//           {" "}
//           Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque, a nostrum.
//           Dolorem, repellat quidem ut, minima sint vel eveniet quibusdam voluptates
//           delectus doloremque, explicabo tempore dicta adipisci fugit amet dignissimos?
//           <br />
//           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur sit
//           commodi beatae optio voluptatum sed eius cumque, delectus saepe repudiandae
//           explicabo nemo nam libero ad, doloribus, voluptas rem alias. Vitae?
//         </div>
//         <div className="actions">
//           <Popup
//             trigger={<button className="button"> Trigger </button>}
//             position="top center"
//             closeOnDocumentClick
//           >
//             <span>
//               Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae
//               magni omnis delectus nemo, maxime molestiae dolorem numquam
//               mollitia, voluptate ea, accusamus excepturi deleniti ratione
//               sapiente! Laudantium, aperiam doloribus. Odit, aut.
//             </span>
//           </Popup>
//           <button
//             className="button"
//             onClick={() => {
//               console.log("modal closed ");
//               close();
//             }}
//           >
//             close modal
//           </button>
//         </div>
//       </div>
//     )}
//   </Popup>
// );