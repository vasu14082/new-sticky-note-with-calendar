import React from 'react';
import { connect } from 'react-redux';
// import _ from 'lodash';
import CalendarEditView from './CalendarEditView';

const OpneEditView = (props) => {
	// console.log("OpneEditView", props);

	return(
		<div style={{marginTop:20, border:"black 1px solid"}}>
			{props.notes.map((note) =>
				props.showCalendarNoteForm.id === note.id && (
				<CalendarEditView key={note.id} note={note} />	)
			)}
		</div>
	)
}

function mapStateToProps(state)
{
	return state;
}

export default connect(mapStateToProps)(OpneEditView);