import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import { changeNoteAndTitle, closeNoteFromCalendar, closeNote } from '../actions';
import _ from 'lodash';

const CalendarEditView = (props) =>
{
	// console.log("CalendarEditView", props);
	const changeTitle = useRef(false);
	const changeNote = useRef(false);
	const changeDate = useRef(false);
	const [showForm, setShowForm] = useState(false);

	function handleEachForm()
	{
		var title = changeTitle.current.value;
		var note = changeNote.current.value;
		var date = changeDate.current.value;
		if( !_.isEmpty(title) && !_.isEmpty(date))
		{
			props.dispatch(changeNoteAndTitle(props.note.id, title, note, date));
			setShowForm(preShowForm => !preShowForm);
		}

	}

	return(
		<div>
			{props.showCalendarNoteForm.id === props.note.id && (
				showForm === true ? (
					<div>
						<input type="text" ref={changeTitle} placeholder="enter title" defaultValue={props.note.title} /><br/>
						<textarea ref={changeNote} placeholder="enter note...." defaultValue={props.note.content}></textarea><br/>
						<input type="date" defaultValue={props.note.date} ref={changeDate} /><br/>
						<input type="button" defaultValue="done" onClick={handleEachForm}/><br/>
						<span onClick={() => {props.dispatch(closeNoteFromCalendar());setShowForm(preShowForm=>!preShowForm)}} style={{color:'red'}}>close</span><br/>
						<span onClick={() => {props.dispatch(closeNote(props.note.id))}} style={{color:'red'}}>Delete</span><br/>
					</div>
				):(
					<div onClick={() => {setShowForm(preShowForm => !preShowForm)}}>
						<label style={{borderBottom:"black 1px solid", fontWeight:'bold'}}>{props.note.title}</label><br/>
						<label>{props.note.content}</label><br/>
						<label>Remind at <span style={{fontWeight:"bold"}}>{props.note.date}</span></label><br/>
						<span onClick={() => {props.dispatch(closeNoteFromCalendar());setShowForm(preShowForm=>!preShowForm)}} style={{color:'red'}}>close</span><br/>
						<span onClick={() => {props.dispatch(closeNote(props.note.id))}} style={{color:'red'}}>Delete</span><br/>
					</div>
				)
			)
		}
		</div>
	)
}

function mapStateToProps(state)
{
	return state;
}

export default connect(mapStateToProps)(CalendarEditView);