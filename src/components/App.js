import React, { useState } from 'react';
// import { addNote } from '../actions';
import AddNote from './AddNote.js';
import ListNote from './ListNote.js';
import Calendar from './calendar/Calendar.js';
import { connect } from 'react-redux';
import OpneEditView from './OpneEditView.js'

const App = (props) => {
const [viewType, setViewType] = useState({viewType:'ListView'});

return(
  <div>
  	<h1>Here is new Sticky notes</h1>
  	<AddNote/>
  	<center>
  		<button onClick={() => {setViewType({viewType:'ListView'})}}>List View</button>
  		<button onClick={() => {setViewType({viewType:'CalendarView'})}}>Calendar View</button>
  	</center>
  	<div style={{width:"50%"}}>
      {viewType.viewType === 'ListView' ? (
          <ListNote/>
        ):(
          <div>
            <Calendar />
            <OpneEditView />
          </div>
        )
      }
  	</div>
  </div>
)
}

function mapStateToProps(state)
{
	return state;
}

export default connect(mapStateToProps)(App)