import React, { useRef } from 'react';
// import { connect } from 'react-redux';
import { changeNoteAndTitle, closeNote, noteEditView } from '../actions';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';

const NoteForm = (props) => {
	// console.log("NoteForm", props);
	const changeTitle = useRef(false);
	const changeNote = useRef(false);
	const changeDate = useRef(false);

	function handleEachForm()
	{
		var title = changeTitle.current.value;
		var note = changeNote.current.value;
		var date = changeDate.current.value;
		if( !_.isEmpty(title) && !_.isEmpty(date))
		{
			props.dispatch(changeNoteAndTitle(props.note.id, title, note, date));
			props.dispatch(noteEditView());
		}

	}

	// const [note, setNote] = uesState(props.note.content);
	return(
		<div style={{marginTop:20, border:"black 1px solid"}}>
		{props.showEditNoteForm.id === props.note.id ? (
			<div>
				<input type="text" ref={changeTitle} placeholder="enter title" defaultValue={props.note.title} /><br/>
				<textarea ref={changeNote} placeholder="enter note...." defaultValue={props.note.content}></textarea><br/>
				<input type="date" defaultValue={props.note.date} ref={changeDate} /><br/>
				<input type="button" value="done" onClick={handleEachForm}/><br/>
				<span onClick={() => props.dispatch(noteEditView())} style={{color:'red'}}>close</span><br/>
				<span onClick={() => props.dispatch(closeNote(props.note.id))} style={{color:'red'}}>Delete</span><br/>
			</div>
			):(
			<div onClick={() => {props.dispatch(noteEditView(props.note.id))}}>
				<label style={{borderBottom:"black 1px solid", fontWeight:'bold'}}>{props.note.title}</label><br/>
				<label>{props.note.content}</label><br/>
				<label>Remind at <span style={{fontWeight:"bold"}}>{props.note.date}</span></label><br/>
				<span onClick={() => props.dispatch(noteEditView())} style={{color:'red'}}>close</span><br/>
				<span onClick={() => props.dispatch(closeNote(props.note.id))} style={{color:'red'}}>Delete</span><br/>
			</div>
			)
		}
		</div>
	)
}

NoteForm.propTypes = {
	note: PropTypes.shape({
	    id:PropTypes.number,
	    title: PropTypes.string,
	    content: PropTypes.string,
	    date:PropTypes.string,
  	}),
	showEditNoteForm:PropTypes.object,
}

const mapStateToProps = (state) => {
  return state;
}

export default connect(mapStateToProps)(NoteForm);