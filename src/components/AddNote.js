import React, { useState, useRef } from 'react'
import { connect } from 'react-redux'
import { addNote } from '../actions'
import _ from 'lodash';

const AddNote = ({ dispatch }) => {
  const [ showForm, setShowForm ] = useState(false);
  const inputTitle = useRef(null);
  const inputNote = useRef(null);
  const inputDate = useRef(null);

  function handleFormsubmit(e){
    e.preventDefault(); 
    var title = inputTitle.current.value;
    var note = inputNote.current.value;
    var date = inputDate.current.value;
    // console.log("date", date);

    if( !_.isEmpty(title) && !_.isEmpty(date))
    {
      dispatch(addNote(title, note, date));
    }
    setShowForm(preShowForm => !preShowForm);
  }
  // const []
  let input;
  return (
    <div>
    {
      showForm ? (
        <form
          onSubmit={handleFormsubmit}
        >
          <input name="title" placeholder="title" ref={inputTitle} /><br/>
          <textarea type="textarea" placeholder="note here..." ref={inputNote}></textarea><br/>
          <input type="date" name="date" ref={inputDate} /><br />
          <button type="submit">done</button>
        </form>
      ):(
        <button
          onClick={()=>setShowForm(preShowForm => !preShowForm)}
        >add note</button>
      )
    }
    </div>
  )
}
export default connect()(AddNote)