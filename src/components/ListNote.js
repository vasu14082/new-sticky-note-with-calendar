import React from 'react'
import { connect } from 'react-redux'
import NoteForm from './NoteForm.js'

const ListNote = (props) => {
	// console.log("in list note component",props);

	return(
		<div>
			{props.notes.lenght === 0 ? (
				<h1>no notes available</h1>
			):(
			props.notes.map(note => 
					<NoteForm note={note} key={note.id} />
					)
			)}
		</div>
	)
}

const mapStateToProps = (state) => {
  return state;
}

export default connect(mapStateToProps)(ListNote);