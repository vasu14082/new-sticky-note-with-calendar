import React from 'react';
import { connect } from 'react-redux';
// import { openNoteFromCalendar } from '../../actions';
import { calendarEditView } from '../../actions';

function CreateNoteInTable({noteDate, dispatch})
{
	return(
		<span onClick={() => {dispatch(calendarEditView(noteDate.id))}}>{noteDate.title}-</span>
	)
}

function mapStateToProps(state)
{
  return state;
}

export default connect(mapStateToProps)(CreateNoteInTable);