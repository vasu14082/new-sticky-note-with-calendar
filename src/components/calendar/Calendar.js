import React, {useState} from 'react';
import Dates from './Dates';
import Popup1 from '../Popup1.js';

function Calendar() {
  const date = new Date();
  const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thus', 'Fri', 'Sat'];
  const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  const [currentMonth, setCurrentMonth] = useState(date.getMonth());
  const [currentYear, setCurrentYear] = useState(date.getFullYear());
  const [showPopup, setShowPopup] = useState(false);
  const [noteDate, setNoteDate] = useState(0);
  const [month, setMonth] = useState(0);
  const [year, setYear] = useState(0);

  function nextMonth() {
    setCurrentYear(prev => (currentMonth === 11) ? prev + 1 : prev);
    setCurrentMonth (prev => (prev + 1) % 12);
  }
  
  function prevMonth() {
    setCurrentYear(prev => (currentMonth === 0) ? prev - 1 : prev);
    setCurrentMonth (prev => (prev === 0 ? 11 : prev - 1) );
  }

  function handleBlankDate(e){
  	setShowPopup(prev => !prev);
  	setNoteDate(e.target.innerHTML);
  	setMonth(currentMonth);
  	setYear(currentYear);
  }

  function togglePopup(){
  	setShowPopup(prev => !prev);
  }

  return (
    <div className="App">
      <div className="card">
      <h3 className="card-header">{months[currentMonth]} {currentYear}</h3>
      {/* <h3>{currentMonth}</h3>
      <h3>{currentYear}</h3> */}
      <h3 onClick={nextMonth}>Next</h3>
      <h3 onClick={prevMonth}>Previous</h3>
        <table style={{width:"100%"}}>
          <thead>
            <tr>
              {days.map((day, index) => (<th key={index}>{day}</th>))}
            </tr>
          </thead>
          <tbody>
            <Dates currentMonth={currentMonth} currentYear={currentYear} handleBlankDate={handleBlankDate}/>
          </tbody>
        </table>
        { showPopup && <Popup1 date={noteDate} month={month} year={year} handleBlankDate={handleBlankDate} togglePopup={togglePopup} />}
      </div>
    </div>
  );
}

export default Calendar;
