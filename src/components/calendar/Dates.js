import React from 'react';
import CreateNoteInTable from './CreateNoteInTable';
import { connect } from 'react-redux';
import _ from 'lodash';


function Dates({currentYear, currentMonth, notes, handleBlankDate}) {
    // console.log("dates", handleBlankDate);
    var noteDates = [];
    //
    notes.map((note)=>{
      let d = new Date(note.date)
      let noteYear = d.getFullYear();
      let noteMonth = d.getMonth();
      let noteDate = d.getDate();
      // console.log(noteYear, noteMonth, currentYear, currentMonth);
      if(noteYear === currentYear && noteMonth === currentMonth)
      {
        noteDates.push({id:note.id, title:note.title, date:noteDate});
      }
      return noteDates;
    });
    // console.log("dates", noteDates);
    let firstDay = (new Date(currentYear, currentMonth)).getDay();
    let daysInMonth = (32 - new Date(currentYear, currentMonth, 32).getDate());
    // console.log(currentYear, currentMonth, daysInMonth);

    let date = 1;

    let table = []

    for (let i = 0; i < 6; i++) {
      // creates a table row
      let children = []
      //creating individual cells, filing them up with data.
      for (let j = 0; j < 7; j++) {
        if (i === 0 && j < firstDay) {
          // empty row
          children.push(<td key={j+""+i}></td>);
        }
        else if (date > daysInMonth) {
          break;
        }
        else {
          // actual row
          let ele = [];
          noteDates.map((noteDate, index) => {
            if(noteDate.date === date)
            {
              ele.push(<CreateNoteInTable key={noteDate.id} noteDate={noteDate} />);
            }
            return ele;
          });
          // console.log("ele",_.isEmpty(ele));
          if(!_.isEmpty(ele))
          {
            children.push(<td key={j+""+i}>{ele}{date}</td>);
          }else{
            children.push(<td onClick={handleBlankDate} key={j+""+i}>{date}</td>);
          }
          date++;
        }
      }
      table.push(<tr key={i}>{children}</tr>)
    }
    return table;
}

function mapStateToProps(state)
{
  return state;
}

export default connect(mapStateToProps)(Dates);