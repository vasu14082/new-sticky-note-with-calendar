let nextNoteId = 0;

export const addNote = ( title, note, date )=> ({
  type: 'ADD_NOTE',
  id:nextNoteId,
  title:title,
  note:note,
  date:date,
})

export const changeNoteAndTitle = ( id,title, note, date )=> ({
  type: 'CHANGE_NOTE',
  id:id,
  title:title,
  note:note,
  date:date,
})

export const closeNote = (id) => ({
  type: 'CLOSE_NOTE',
  id:id,
})

export const openNoteFromCalendar = (id) => ({
  type: 'OPEN_NOTE_FROM_CALENDAR',
  id:id,
});

export const noteEditView = (id) => ({
  type: 'NOTE_EDIT_VIEW',
  id:id,
});

export const calendarEditView = (id) => ({
  type: 'CALENDAR_EDIT_VIEW',
  id,
});

export const closeNoteFromCalendar = () => ({
  type: 'CLOSE_NOTE_FROM_CALENDAR',
});