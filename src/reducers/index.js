import { createStore } from 'redux';
import notes from './notes.js';


let initialState = {
	notes:[],
	showEditNoteForm:{id:null},
	showCalendarNoteForm:{id:null},
};
const persistedState = localStorage.getItem('reduxState');
if (persistedState) {
  initialState = JSON.parse(persistedState)
}

const rootReducer = createStore(notes, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

rootReducer.subscribe(() => {
  localStorage.setItem('reduxState', JSON.stringify(rootReducer.getState()))
});


export default rootReducer