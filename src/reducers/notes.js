const notes = (state = [], action) => {
    // console.log("in notes reducer", action)
  var new_state;
  switch (action.type) {
    case 'ADD_NOTE':
      new_state = {...state};
      let setId = 0;
      var last_obj = new_state.notes[new_state.notes.length-1];
      if(last_obj)
      {
        setId = last_obj.id;
        setId++;
      }
      return {
        ...state,
        notes:[...state.notes, {id:setId, title:action.title,content:action.note, date:action.date}],
      }

    case 'CHANGE_NOTE':
        const index = state.notes.findIndex(post => post.id === action.id);
        new_state = {
          ...state,
          notes:[...state.notes.slice(0,index),{id:action.id,title:action.title,content:action.note,date:action.date}, ...state.notes.slice(index+1) ]
        };
      return new_state;

    case 'CLOSE_NOTE':
      new_state ={
        ...state,
        notes:[...state.notes.filter((item) => item.id !== action.id)]
      };
      return new_state;
    case 'NOTE_EDIT_VIEW':
      var newObj = {id:null};
      if(action.id !== undefined)
      {
        newObj = {id:action.id};
      }
      new_state ={
        ...state,
        showEditNoteForm:newObj,
      };
      return new_state;
    case 'CALENDAR_EDIT_VIEW':
      var newObj1 = {id:null};
      if(action.id !== undefined)
      {
        newObj1 = {id:action.id};
      }
      new_state ={
        ...state,
        showCalendarNoteForm:newObj1,
      };
      return new_state;
    case 'CLOSE_NOTE_FROM_CALENDAR':
      new_state = {
        ...state,
        showCalendarNoteForm:{id:null},
      };
      return new_state;
    default:
      return state
  }
}
export default notes